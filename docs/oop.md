## <a name='what-is-oop'>Python에서 객체 지향 프로그래밍이란?</a>
객체 지향 프로그래밍은 속성과 동작이 개별 **객체**에 번들되도록 프로그램을 구조화하는 수단을 제공하는 [프로그래밍 패러다임](http://en.wikipedia.org/wiki/Programming_paradigm)이다.

예를 들어, 객체는 이름, 나이, 주소와 같은 **속성**과 걷기, 말하기, 숨쉬기, 달리기와 같은 **행동**을 가진 사람을 나타낼 수 있다. 또는 수신자 목록, 제목 및 본문과 같은 속성과 첨부 파일 추가 및 보내기와 같은 동작을 가진  [이메일](https://realpython.com/python-send-email/)을 나타낼 수 있다.

즉, 객체 지향 프로그래밍은 자동차와 같은 구체적이고 실제적인 것뿐만 아니라 회사와 직원, 학생과 교사간의 관계를 모델링하기 위한 접근법이다. OOP는 실제 엔터티에 일부 데이터가 연결되어 있고 특정 기능을 수행할 수 있는 소프트웨어 객체로 모델링한다.

또 다른 일반적인 프로그래밍 패러다임은 프로그램을 구성하는 **절차적 프로그래밍**으로, 이는 프로그램이 작업을 완료하기 위해 순차적으로 흐르는 함수와 코드 블록의 형태로 표현한다는 점에서 요리의 레시피와 같다.

핵심은 객체가 파이썬에서 객체 지향 프로그래밍의 중심에 있으며, 절차적 프로그래밍에서와 같이 데이터를 나타낼 뿐만 아니라 프로그램의 전반적인 구조에서도 유사하다는 것이다.

## <a name='define-class'>Python에서 클래스 정의</a>
숫자, 문자열 및 목록과 같은 기본 [데이터 구조](https://realpython.com/courses/python-data-types/)는 각각 사과의 가격, 시의 이름 또는 좋아하는 색과 같은 간단한 정보를 나타내도록 설계되었습니다. 더 복잡한 것을 표현하고 싶다면?

예를 들어 조직의 직원을 추적한다고 가정해 보겠다. 각 직원의 이름, 나이, 직책, 근무를 시작한 연도와 같은 기본 정보를 저장해야 한다.

이 작업을 수행하는 한 가지 방법은 각 직원 정보를 [리스트(list)](https://realpython.com/python-lists-tuples/)로 표시하는 것이다.

```python
kirk = ["James Kirk", 34, "Captain", 2265]
spock = ["Spock", 35, "Science Officer", 2254]
mccoy = ["Leonard McCoy", "Chief Medical Officer", 2266]
```

이 접근 방법에는 여러 문제가 있다.

첫째로, 그것은 더 큰 코드 파일로 관리를 어렵게 만들 수 있다. `kirk` 리스트가 선언된 곳에서 몇 줄 떨어진 곳에서 `kirk[0]`을(를) 참조하는 경우 인덱스 `0`의 요소가 직원 이름임을 기억할 수 있을까?

둘째, 모든 직원이 리스트에 동일한 수의 요소를 가지고 있지 않은 경우 오류가 발생할 수 있다. 위의 `mccoy` 리스트에서 나이가 누락되었으므로 `mccoy[1]`은 `Dr.McCoy's`의 나이 대신 "Chief Mdeical Office"를 반환할 것이다.

이러한 유형의 코드를 보다 관리하기 쉽고 유지 관리하기 쉽게 만드는 좋은 방법은 **클래스**를 사용하는 것이다.

### <a name='classes-vs-instances'>클래스와 인스탄스</a>
클래스는 사용자 정의 데이터 구조를 만드는 데 사용된다. 클래스는 **메서드**라고 하는 함수를 정의한다. 메서드는 클래스에서 생성된 객체가 그것의 데이터로 수행할 수 있는 동작과 작업을 정의한다.

이 튜토리얼에서는 개별 개가 가질 수 있는 속성과 행동에 대한 일부 정보를 저장하는 `Dog` 클래스를 만든다.

클래스는 어떤 것이 어떻게 정의되어야 하는지에 대한 청사진이다. 실제로는 어떤 데이터도 포함하지 않는다. `Dog` 클래스는 개를 정의하는 데 이름 또는 나이가 필요하지만 특정 개의 이름이나 나이는 포함하지 않는다.

클래스가 청사진인 반면 **인스턴스**는 클래스에서 빌드되고 실제 데이터를 포함하는 객체이다. `Dog` 클래스의 인스턴스는 더 이상 청사진이 아니다. 네 살짜리 Miles라는 이름을 가진 실제 개이다.

다른 말로 하자면, 클래스는 양식이나 설문지와 같다. 인스턴스는 정보기 채워진 양식과 같다. 많은 사람들이 자신만의 고유한 정보로 동일한 양식을 작성할 수 있는 것처럼, 단일 클래스에서 많은 인스턴스를 만들 수 있다.

### <a name='how-to-define-class'>클래스 정의</a>
모든 클래스 정의는 `class` 키워드로 시작하고 클래스 이름과 콜론(`:`)이 나온다. 클래스 정의 아래에 들여쓰기된 코드는 클래스 본문의 일부이다.

다음은 `Dog` 클래스의 예이다.

```python
class Dog:
    pass
```

`Dog` 클래스의 본문은 `pass` 키워드라는 단일 문으로 구성되어 있다. `pass`는 종종 코드를 최종적으로 어디에서 수행할 것인지를 나타내는 자리 표시자로 사용된다. Python에서 오류 발생없이 이 코드를 실행할 수 있다.

> **Note:** Python 클래스 이름은 관례적으로 CapitalizedWords 표기법으로 작성된다. 예를 들어, Jack Russell Terrier와 같은 특정 품종의 개를 위한 클래스는 JackRussellTerrier로 쓰여진다.

`Dog` 클래스는 지금 별로 흥미롭지 않다. 모든 `Dog` 객체가 가져야 할 몇 가지 속성을 정의하여 좀 더 다듬어 보겠다. 이름, 나이, 털 색상, 품종을 포함하여 우리가 선택할 수 있는 많은 속성들이 있다. 단순하게 하기 위해 이름과 나이만 사용한다.

`.__init__()`라는 메서드를 이용하여 모든 `Dog` 객체가 가져야 하는 속성을 정의할 수 있다. 새 `Dog` 객체가 생성될 때마다 `.__init__()`는 객체의 속성 값을 할당하여 객체의 초기 **상태**를 설정한다. 즉, `.__init__()`는 클래스의 각 새 인스턴스를 초기화한다.

`.__init__()`에 임의의 수의 매개 변수를 줄 수 있지만, 첫 번째 매개 변수는 항상 `self`라는 [변수](https://realpython.com/python-variables/)이어야 한다. 새 클래스 인스턴스가 생성되면 인스턴스가 `.__init__()`의 `self` 매개 변수로 자동 전달되므로 객체에 새 **속성**을 정의할 수 있다.

`.name`과 `.age` 속성을 생성하는 `.__init__()` 메서드로 `Dog` 클래스를 수정하자.

```python
class Dog:
    def __init__(self, name, age):
        self.name = name
        self.age = age
```

`.__init__()` 메서드의 시그니처는 4개의 공백을 들여쓴다. 메서드의 본문은 8개의 공백으로 들여쓴다. 이 들여쓰기는 매우 중요하다. Python에게 `.__init__()` 메서드가 `Dog` 클래스에 속함을 알려준다.

`.__init__()`의 본문에는 `self` 변수를 사용하는 두 문장이 있다.

1. **self.name = name**은 `name`이라는 속성을 생성하고 `name` 매개 변수의 값을 할당한다.
2. **self.age = age**는 `age`라는 속성을 생성하고 `age` 매개 변수의 값을 할당한다.

`.__init__()`에서 생성된 속성을 인스턴스 속성이라고 한다. **인스턴스 속성(instance attribute)** 값은 클래스의 특정 인스턴스에 한정된다. 모든 `Dog` 객체에는 이름과 나이가 있지만 이름과 나이 속성의 값은 Dog 인스턴스에 따라 다르다.

반면, **클래스 속성(class attribute)**은 모든 클래스 인스턴스에 대해 동일한 값을 갖는 속성이다. `.__init__()` 외부의 [변수](https://realpython.com/python-variables/) 이름에 값을 할당하여 클래스 속성을 정의할 수 있다.

예를 들어, 다음 `Dog` 클래스는 "Canis familiaris"라는 값을 가진 `species`라는 클래스 속성을 갖는다.

```python
class Dog:
    # Class attribute
    species = "Canis familiaris"

    def __init__(self, name, age):
        self.name = name
        self.age = age
```

클래스 속성은 클래스 이름의 첫 번째 줄 바로 아래에서 정의되며 네 개의 공백으로 들여쓴다. 항상 초기 값을 할당해야 한다. 클래스 인스턴스가 생성되면 클래스 속성이 자동으로 생성되어 초기 값으로 할당된다.

클래스 속성을 사용하여 모든 클래스 인스턴스에 대해 동일한 값을 가져야 하는 속성을 정의한다. 인스턴스마다 다른 속성에 대해 인스턴스 속성을 사용한다.

이제 `Dog` 클래스가 있으니, 개를 만들어 보자!

## <a name='instantiate-object'>Python에서 객체 인스탄스화</a>
IDLE의 대화형 창을 열고 다음을 입력한다.

```python
>>> class Dog:
...     pass
```

그러면 속성이나 메서드가 없는 새 `Dog` 클래스가 만들어진다.

클래스에서 새 객체를 만드는 것을 객체를 **인스턴스화(instantiating)** 한다고 부른다. 클래스 이름을 입력한 후 소괄호를 열고 닫는 방법으로 새 `Dog` 객체를 인스턴스화할 수 있다.

```python
>>> Dog()
<__main__.Dog object at 0x106702d30>
```

이제 `0x106702d30`에 새 `Dog` 객체가 생성되었다. 이 재미있게 생긴 문자와 숫자 문자열은 컴퓨터의 메모리에서 `Dog` 객체가 저장된 위치를 나타내는 메모리 주소이다. 화면에 표시되는 주소는 서로 다를 것이다.

이제 두 번째 `Dog` 객체를 인스턴스화한다.

```python
>>> Dog()
<__main__.Dog object at 0x0004ccc90>
```

새 `Dog` 인스턴스는 다른 메모리 주소에 있다. 그것은 완전히 새로운 인스턴스이고 여러분이 인스턴스화한 첫 번째 `Dog` 객체와는 완전히 다른 객체이기 때문이다.

이 문제를 다른 방법으로 보려면 다음을 입력하시오.

```python
>>> a = Dog()
>>> b = Dog()
>>> a == b
False
```

이 코드에서 두 Dog 객체를 새로이 생성하고 변수 `a`와 `b`에 할당한다. `==` 연산자를 사용하여 `a`와 `b`를 비교하면 결과는 `False`이다. `a`와 `b`는 둘 다 `Dog` 클래스의 인스턴스이지만 메모리에 있는 두 객체를 나타낸다.

### <a name='class-and-instance-attribute'>클래스와 인스탄스 속성</a>
이제 `.species`라는 클래스 속성과 `.name` 및 `.age`라는 두 인스턴스 속성을 사용하여 새 `Dog` 클래스를 만든다.

```python
>>> class Dog:
...     species = "Canis familiaris"
...     def __init__(self, name, age):
...         self.name = name
...         self.age = age
```

이 `Dog` 클래스의 객체를 인스턴스화하려면 `name`과 `age` 값을 제공해야 한다. 그렇지 않으면 Python은 `TypeError`를 발생시킨다.

```python
>>> Dog()
Traceback (most recent call last):
  File "<pyshell#6>", line 1, in <module>
    Dog()
TypeError: __init__() missing 2 required positional arguments: 'name' and 'age'
```

name과 age 매개 변수에 인수를 전달하려면 클래스 이름 뒤에 있는 소괄호에 값을 넣는다.

```python
>>> buddy = Dog("Buddy", 9)
>>> miles = Dog("Miles", 4)
```

위에서 새로운 두 `Dog` 인스턴스를 만들었다. 하나는 Buddy라는 이름의 9살인 개이고 다른 하나는 Miles라는 이름의 4살인 개이다.

`Dog` 클래스의 `.__init__()` 메서드에는 세 매개 변수가 있는데, 예제에서 두 개의 인수만 전달되는 이유는 왜일까?

`Dog` 개체를 인스턴스화하면 Python은 새 인스턴스를 생성하여 `.__init__()`의 첫 번째 매개 변수로 전달한다. 이렇게 하면 기본적으로 자체 매개 변수가 제거되므로 `name`과 `age` 매개 변수만 신경쓰면 된다.

`Dog` 인스턴스를 만든 후에는 **dot 표기법(dot notation)**을 사용하여 해당 인스턴스 속성을 액세스할 수 있다.

```python
>>> buddy.name
'Buddy'
>>> buddy.age
9

>>> miles.name
'Miles'
>>> miles.age
4
```

다음과 같은 방법으로 클래스 속성을 액세스할 수 있다.

```python
>>> buddy.species
'Canis familiaris'
```

클래스를 사용하여 데이터를 구성하는 가장 큰 이점 중 하나는 인스턴스가 사용자가 기대하는 속성을 갖도록 보장된다는 것이다. 모든 `Dog` 인스턴스에는 `.species`, `.name`과 `.age` 속성이 있으므로 이러한 속성은 항상 값을 반환하므로 안심하고 사용할 수 있다.

속성이 존재하는 것이 보장되지만 값은 동적으로 변경될 수 있다.

```python
>>> buddy.age = 10
>>> buddy.age
10

>>> miles.species = "Felis silvestris"
>>> miles.species
'Felis silvestris'
```

이 예에서는 `buddy` 객체의 `.age` 속성을 `10`으로 변경한다. 그런 다음 `miles` 객체의 `.sepece` 속성을 고양이의 일종인 "Felis silvestris"로 변경한다. 그것은 `miles`를 아주 이상한 개로 만들지만, 그것은 Python에서 유효하다!

여기서 중요한 점은 사용자 지정 객체는 기본적으로 변경할 수 있다는 것이다. 동적으로 변경할 수 있는 객체는 변경할 수 있다. 예를 들어 리스트과 [딕셔너리](https://realpython.com/python-dicts/)는 변경할 수 있지만 문자열과 튜플은 [변경할 수 없다](https://realpython.com/courses/immutability-python/).

### <a nmae='instance-method'>인스탄스 메서드</a>
**인스턴스 메소드**는 클래스 내부에 정의되어 있으며 해당 클래스의 인스턴스에서만 호출할 수 있는 함수이다. `.__init_()`같이 인스턴스 메서드의 첫 번째 매개 변수는 항상 `self`이다.

IDLE에서 새 편집기 창을 열고 다음 `Dog` 클래스를 입력한다.

```python
class Dog:
    species = "Canis familiaris"

    def __init__(self, name, age):
        self.name = name
        self.age = age

    # Instance method
    def description(self):
        return f"{self.name} is {self.age} years old"

    # Another instance method
    def speak(self, sound):
        return f"{self.name} says {sound}"
```

이 `Dog` 클래스에는 두 인스턴스 메서드가 있다.

1. **.description()**은 개의 name과 age를 표시하는 문자열을 반환한다.
2. **.speak()**는 `sound`라는 매개 변수가 하나 있으며, 개의 name과 개가 내는 sound를 포함하는 문자열을 반환한다.

수정된 `Dog` 클래스를 `dog.py`라는 파일에 저장하고 F5를 눌러 프로그램을 실행한다. 그런 다음 대화형 창을 열고 다음을 입력하여 인스턴스 메서드의 동작을 확인하시오.

```python
>>> miles = Dog("Miles", 4)

>>> miles.description()
'Miles is 4 years old'

>>> miles.speak("Woof Woof")
'Miles says Woof Woof'

>>> miles.speak("Bow Wow")
'Miles says Bow Wow'
```

위의 `Dog` 클래스에서 `.description()`은 `Dog` 인스턴스 `miles`에 대한 정보를 포함하는 문자열을 반환한다. 자신의 클래스를 작성할 때 클래스 인스턴스에 대한 유용한 정보를 포함하는 문자열을 반환하는 메서드를 사용하는 것이 좋다. 그러나 `.description()`이 가장 Pythonic 방법은 아니다.

리스트 객체를 만들 때 `print()`를 사용하여 리스트와 유사한 문자열을 표시할 수 있다.

```python
>>> names = ["Fletcher", "David", "Dan"]
>>> print(names)
['Fletcher', 'David', 'Dan']
```

`miles` 객체를 `print()`할 때 어떤 일이 일어나는지 살펴보겠다.

```python
>>> print(miles)
<__main__.Dog object at 0x00aeff70>
```

`print(miles)`할 때, 메모리 주소 `0x00aeff70`에 `miles`가 `Dog` 객체라는 암호화된 메시지가 나타난다. 이 메시지는 별로 도움이 되지 않는다. `.__str__()`이라는 특수 인스턴스 메서드를 정의하여 인쇄할 항목을 바꿀 수 있다.

편집기 창에서 `Dog` 클래스의 `.description()` 메서드의 이름을 `.__str__()`로 변경한다.

```python
class Dog:
    # Leave other parts of Dog class as-is

    # Replace .description() with __str__()
    def __str__(self):
        return f"{self.name} is {self.age} years old"
```

파일을 저장하고 F5를 누른다. 이제 `print(miles)`할 때 훨씬 더 친숙한 출력을 얻을 수 있다.

```python
>>> miles = Dog("Miles", 4)
>>> print(miles)
'Miles is 4 years old'
```

`.__init_()`와 `._str__()` 같은 메서드를 이중 밑줄로 시작하고 끝나기 때문에 **dunder 메서드** 라고 한다. 메서드에서  Python에서 클래스를 지정하는 데 사용할 수 있는 많은 던더 메서드가 있다. Python 책을 시작하는 시점에 주제가 너무 앞서 있지만, 던더 메서드를 이해하는 것은 Python에서 객체 지향 프로그래밍을 마스터하는 데 중요한 부분이다.

다음 섹션에서는 지식을 한 단계 발전시켜 다른 클래스의 클래스를 만드는 방법에 대해 설명합니다.

### <a name='check-your-understanding'>연습</a>

**Exercise:**  `Car` 클래스를 생성한다.

두 개의 인스턴스(instance) 속성을 갖는 `Car` 클래스를 만든다.

1. `.color`, 자동차 색상을 문자열로 저장한다.
2. `.mileage`, 자동차의 마일리지를 정수로 저장한다.

그런 다음 2만 마일을 주행한 파란색 자동차와 3만 마일을 주행한 빨간색 자동차의 두 차를 인스턴스화하고 색상과 주행 거리를 인쇄한다. 출력은 다음과 같아야 한다.

```
The blue car has 20,000 miles.
The red car has 30,000 miles.
```

**Solution:** `Car` 클래스를 생성한다.

먼저 `.color`와 `.mileage` 인스턴스(instance) 속성을 갖는 `Car` 클래스를 만든다.

```python
class Car:
    def __init__(self, color, mileage):
        self.color = color
        self.mileage = mileage
```

`.__init__()`의 색상과 마일리지 매개 변수가 `self.color`와 `self.mile`에 각각 할당되어 두 인스턴스 속성을 생성할 수 있다.

이제 두 `Car` 인스턴스를 생성할 수 있다.

```python
blue_car = Car(color="blue", mileage=20_000)
red_car = Car(color="red", mileage=30_000)
```

`blue_car` 인스턴스는 "blue" 값을 색상 매개 변수에 전달하고 20_000을 마일리지 매개 변수에 전달하여 생성된다. 마찬가지로, `red_car`는 "red"와 30_000 값으로 생성된다.

각 `Car` 객체의 색상과 마일리지를 인쇄하려면 두 객체가 모두 포함된 튜플에 루프를 적용할 수 있다.

```python
for car in (blue_car, red_car):
    print(f"The {car.color} car has {car.mileage:,} miles")
```

위의 `for` 루프의 [f-string](https://realpython.com/python-f-strings/)은 `.color`와 `.mileage` 속성을 문자열에 삽입하고 `:` [형식 지정자(format specifier)](https://realpython.com/python-formatted-output/#f-string-formatting)를 사용하여 천 단위로 그룹화하여 `,`로 구분된 마일리지를 인쇄한다.

최종 출력은 다음과 같다.

```
The blue car has 20,000 miles.
The red car has 30,000 miles.
```

준비가 되었으면 다음 섹션으로 이동합니다.

## <a name='inherit-from-other-classes'>Python에서 다른 클래스로부터 상속</a>
[상속](https://realpython.com/inheritance-composition-python/)은 한 클래스가 다른 클래스의 속성과 메서드를 취하는 프로세스이다. 새로 형성된 클래스를 **자식 클래스(child class)**라고 하며, 자식 클래스를 파생한 클래스를 **부모 클래스(parent class)**라고 한다.

> **Note:** 이 튜토리얼은 [Python Basics: A Practical Introduction to Python 3](https://realpython.com/products/python-basics-book/)의 "Object-Oriented Programming (OOP)" 장에서 가져왔다. 만약 여러분이 읽고 있다면, [책의 나머지 부분](https://realpython.com/products/python-basics-book/)을 꼭 확인하세요.

자식 클래스는 부모 클래스의 속성과 메서드를 재정의하거나 확장할 수 있다. 즉, 하위 클래스는 상위 클래스의 모든 속성과 메서드를 상속하지만 자신의 고유한 속성과 메서드를 지정할 수도 있다.

비록 비유가 완벽하지는 않지만, 여러분은 객체 유전을 유전학의 유전과 같은 것으로 생각할 수도 있다.

여러분은 어머니로부터 머리 색깔을 물려받았을지도 모른다. 그건 타고난 속성이며, 여러분이 머리를 보라색으로 염색하기로 결정했다고 가정해 보자. 여러분의 어머니의 머리카락이 보라색이 아니라고 가정할 때, 여러분은 여러분이 어머니로부터 물려받은 머리카락 색 속성을 **무시(override)**한 것이다.

여러분은 또한 어떤 의미에서 여러분의 언어를 부모님으로부터 물려받는다. 만약 여러분의 부모님이 영어를 하신다면, 여러분도 영어를 할 수 있다. 이제 여러분이 독일어와 같은 제2 외국어를 배우기로 결정했다고 상상해 보자. 이 경우에는 부모님이 가지고 있지 않은 속성을 추가했기 때문에 속성을 **확장(extend)**한 것이다.

### <a name='dog-park-example'>애완견 공원 예</a>
잠시 동안 여러분이 애완견 공원에 있다고 상상해 보자. 공원에는 다양한 품종의 개들이 많이 있는데, 모두 다양하게 개 행동을 한다.

이제 Python 클래스를 사용하여 애완견 공원을 모델링하려고 한다. 이전 섹션에서 작성한 `Dog` 클래스는 이름과 나이로 개를 구분할 수 있었지만 품종으로 구분하지 않았다.

`.breed` 속성을 추가하여 편집기 창에서 `Dog` 클래스를 수정할 수 있다.

```python
class Dog:
    species = "Canis familiaris"

    def __init__(self, name, age, breed):
        self.name = name
        self.age = age
        self.breed = breed
```

앞에서 설명한 인스턴스 메서드는 여기서는 중요하지 않기 때문에 생략한다.

수정된 파일을 저장합니다. 이제 대화형 창에서 여러 개의 다른 개를 인스턴스화하여 애완견 파크를 모델링할 수 있다.

```python
>>> miles = Dog("Miles", 4, "Jack Russell Terrier")
>>> buddy = Dog("Buddy", 9, "Dachshund")
>>> jack = Dog("Jack", 3, "Bulldog")
>>> jim = Dog("Jim", 5, "Bulldog")
```

개의 품종마다 조금씩 다른 행동을 한다. 예를 들어, 불독은 *woof*처럼 들리는 낮게 짖는 소리를 가지고 있지만 닥스훈트는 높은 소리의 *yap*같은 소리를 가지고 있다.

`Dog` 클래스만 사용하면 `Dog` 인스턴스에서 `.speak()`의 `sound` 인수를 호출할 때마다 문자열을 제공해야 한다.

```python
>>> buddy.speak("Yap")
'Buddy says Yap'

>>> jim.speak("Woof")
'Jim says Woof'

>>> jack.speak("Woof")
'Jack says Woof'
```

`.speak()`의 모든 호출에 문자열을 전달하는 것은 반복적이고 불편하다. 또한 각 `Dog` 인스턴스에서 발생하는 소리를 나타내는 문자열은 `.breed` 속성으로 결정해야 하지만 여기서는 호출될 때마다 올바른 문자열을 `.speak()`에 수동으로 전달해야 한다.

각 견종에 대한 하위 클래스를 작성하여 견종 작업 경험을 단순화할 수 있다. 이를 통해 `.speak()`에 대한 기본 인수 지정을 포함하여 각 자식 클래스가 상속하는 기능을 확장할 수도 있다.

### <a name='parent-classes-vs-child-classes'>부모 클래스와 자식 클래스</a>
위에서 언급한 세 가지 품종 Jack Russell Terrier, Dachshund과 Bulldog 각각에 대해 자식 클래스를 만들어 보자.

참고로 `Dog` 클래스의 전체 정의는 다음과 같다.

```python
class Dog:
    species = "Canis familiaris"

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __str__(self):
        return f"{self.name} is {self.age} years old"

    def speak(self, sound):
        return f"{self.name} says {sound}"
```

하위 클래스를 만들려면 고유한 이름으로 새 클래스를 만든 다음 부모 클래스의 이름을 소괄호 안에 넣는다. 다음을 `dog.py` 파일에 추가하여 `Dog` 클래스의 새로운 세 자식 클래스를 만든다.

```python
class JackRussellTerrier(Dog):
    pass

class Dachshund(Dog):
    pass

class Bulldog(Dog):
    pass
```

파일을 저장하고 실행한다. 자식 클래스를 정의하면 이제 대화형 창에서 특정 품종의 개를 인스턴스화할 수 있다.

```python
>>> miles = JackRussellTerrier("Miles", 4)
>>> buddy = Dachshund("Buddy", 9)
>>> jack = Bulldog("Jack", 3)
>>> jim = Bulldog("Jim", 5)
```

하위 클래스의 인스턴스는 상위 클래스의 모든 특성 및 메서드를 상속받는다.

```python
>>> miles.species
'Canis familiaris'

>>> buddy.name
'Buddy'

>>> print(jack)
Jack is 3 years old

>>> jim.speak("Woof")
'Jim says Woof'
```

지정된 개체가 속한 클래스를 확인하려면 내장 함수 `type()`을 이용할 수 있다.

```python
>>> type(miles)
<class '__main__.JackRussellTerrier'>
```

`miles`도 `Dog` 클래스의 인스턴스인지 확인하려면 어떻게 해야 할까? 내장 함수 `isinstance()`를 사용하여 이를 수행할 수 있다.

```python
>>> isinstance(miles, Dog)
True
```

`instance()`는 두 인수 객체와 클래스를 사용한다. 위 예에서 `isinstance()`는 `miles`이 `Dog` 클래스의 인스턴스인지 확인하고 `True`를 반환한다.

`miles`, `buddy`, `jack`과 `jim` 객체는 모두 `Dog` 인스턴스이지만 `miles`은 `Bulldog` 인스턴스가 아니며 `jack`은 `Dachshund` 인스턴스가 아니다.

```python
>>> isinstance(miles, Bulldog)
False

>>> isinstance(jack, Dachshund)
False
```

보다 일반적으로 자식 클래스에서 생성된 모든 개체는 부모 클래스의 인스턴스이지만 다른 자식 클래스의 인스턴스는 아닐 수도 있다.

이제 여러분이 몇몇 다른 품종의 개들을 위한 자식 클래스을 만들었으니, 각 품종에 고유한 소리를 내도록 합시다.

### <a name='extend-functionality-of-parent-classes'>부모 클래스의 기능 확장</a>
개의 품종마다 짖는 소리가 조금씩 다르기 때문에 각 `.speak()` 메서드의 `sound` 인수에 기본값을 제공하려고 한다. 이렇게 하려면 각 품종에 대한 클래스 정의에서 `.speak()`를 재정의해야 한다.

상위 클래스에 정의된 메서드를 재정의하려면 하위 클래스에 동일한 이름의 메서드를 정의한다. `JackRussellTerrier` 클래스는 다음과 같다.

```python
class JackRussellTerrier(Dog):
    def speak(self, sound="Arf"):
        return f"{self.name} says {sound}"
```

이제 `.speak()`는 `JackRussellTerrier` 클래스에 정의되며 사운드에 대한 기본 인수를 "Arf"로 설정된다.

`dog.py`를 새로운 `JackRussellTerrier` 클래스가 정의되도록 업데이트하고 파일을 저장하고 실행한다. 이제 다음과 같은 인수를 전달하지 않고 `JackRussellTerrier` 인스턴스에서 `.speak()`를 호출할 수 있다.

```python
>>> miles = JackRussellTerrier("Miles", 4)
>>> miles.speak()
'Miles says Arf'
```

때때로 개들은 다른 짖는 소리를 내기 때문에 `miles`가 화가 나서 으르렁거릴 때도 다른 소리로 `.speak()`을 호출할 수 있다.

```python
>>> miles.speak("Grrr")
'Miles says Grrr'
```

클래스 상속과 관련하여 한 가지 유의할 점은 부모 클래스의 변경 내용이 자식 클래스에 자동으로 전파된다는 것이다. 이 문제는 변경 중인 속성 또는 메서드가 하위 클래스에서 재정의되지 않는 한 발생한다.

예를 들어 편집기 창에서 `Dog` 클래스의 `.speak()`에서 반환하는 문자열을 변경한다.

```python
class Dog:
    # Leave other attributes and methods as they are

    # Change the string returned by .speak()
    def speak(self, sound):
        return f"{self.name} barks: {sound}"
```

파일을 저장한다. 이제 `jim`이라는 이름의 새 `Bulldog` 인스턴스를 만들면 `jim.speak()`는 새 문자열을 반환한다.

```python
>>> jim = Bulldog("Jim", 5)
>>> jim.speak("Woof")
'Jim barks: Woof'
```

때때로 부모 클래스에서 메서드를 완전히 재정의하는 것이 바람직하다. 그러나 이 경우에는 `JackRussellTerrier` 클래스가 `Dog.speak()`의 출력 문자열 형식에 대한 변경 사항을 손실하지 않도록 한다.

이렇게 하려면 하위 `JackRussellTerrier` 클래스에서 `.speak()` 메서드를 정의해야 합니다. 그러나 출력 문자열을 명시적으로 정의하는 대신 `JackRussellTerrier.speak()`에 전달한 것과 동일한 인수를 사용하여 자식 클래스의 `.speak()` *내부*에 있는 `Dog` 클래스의 `.speak()`을 호출해야 한다.

[super()](https://realpython.com/python-super/)를 사용하여 하위 클래스의 메서드 내부에서 상위 클래스를 액세스할 수 있다.

```python
class JackRussellTerrier(Dog):
    def speak(self, sound="Arf"):
        return super().speak(sound)
```

`JackRussellTerrier` 내부에서 `super(.speak(sound))`를 호출하면 Python은 부모 클래스인 `Dog`에서 `.speak()` 메서드를 검색하여 매개 변수 `sound`로 호출한다.

`dog.py`을 새로운 `JackRussellTerrier` 클래스로 업데이트하시오. 파일을 저장하고 대화형 창에서 테스트한다.

```python
>>> miles = JackRussellTerrier("Miles", 4)
>>> miles.speak()
'Miles barks: Arf'
```

이제 `miles.speak()`를 호출하면 `Dog` 클래스의 새로운 형식을 반영한 출력이 디스플레이된다.

> **Notes:** 위의 예에서 **클래스 계층**은 매우 간단하다. `JackRussellTerrier` 클래스는 `Dog`라는 부모 클래스 하나가 있다. 실제 사례에서, 클래스 계층 구조는 상당히 복잡해질 수 있다.
>
> `super()`는 단순히 부모 클래스에서 메서드나 속성을 검색하는 것 이상의 작업을 한다. 일치하는 메서드 또는 속성을 위해 클래스 계층 전체를 트래버스(traverse)한다. 조심하지 않으면 `super()`는 놀라운 결과를 초래할 수 있다.

### <a name='check-your-understanding-2'>연습</a>
**문제:** 클래스 상속

`Dog` 클래스로부터 상속받는 `GoldenRetriever` 클래스를 만든다. `GoldenRetriever.speak()`의 `sound` 인수를 기본값인 "Bark"로 지정한다. 상위 `Dog` 클래스에 다음 코드를 사용한다.

```python
class Dog:
    species = "Canis familiaris"

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __str__(self):
        return f"{self.name} is {self.age} years old"

    def speak(self, sound):
        return f"{self.name} says {sound}"
```

**해답:** 래스 상속

`Dog` 클래스에서 상속되고 `.speak()` 메서드를 재정의하는 `GoldenRetriever`라는 클래스를 만든다.

```python
class GoldenRetriever(Dog):
    def speak(self, sound="Bark"):
        return super().speak(sound)
```

`GoldenRetriever.speak()`의 `sound` 매개 변수에 기본값인 "Bark"를 지정한다. 그런 다음 `super()`는 `GoldenRetriever` 클래스의 `.speak()` 메서드와 동일한 인수로 부모 클래스의 `.speak()` 메서드를 호출하는 데 사용된다.

## <a name='conclusion'>결론</a>
이 튜토리얼에서는 Python의 객체 지향 프로그래밍(OOP)에 대해 배웠다. [Java](https://go.java/), [C#](https://docs.microsoft.com/en-us/dotnet/csharp/tour-of-csharp/) 및 [C++](https://www.cplusplus.com/info/description/)같은 대부분의 현대 프로그래밍 언어는 OOP 원칙을 따르므로 어떤 프로그래밍 경력을 지향하든 여기서 얻은 지식을 적용할 수 있을 것이다.

**이 튜토리얼에서는 다음을 수행하는 방법에 대해 배웠다.**

- 객체에 대한 청사진의 일종인 **클래스** 정의
- 클래스에서 **객체** 인스턴스화
- **속성(attribute)**과 **메서드**를 사용하여 개체의 **속성(property)**과 **동작** 정의
- **상속**을 사용하여 **부모** 클래스에서 **자식** 클래스 만들기
- **`super()`**를 사용하여 부모 클래스의 메서드 참조
- **`isinstance()`**를 사용하여 객체가 다른 클래스에서 상속되었는지 확인

만약 당신이 [Python Basics: A Practical Introduction to Python 3]()로부터 이 샘플에서 배운 것이 유용히다는 생각이 들면, [책의 나머지 부분](https://realpython.com/products/python-basics-book/)을 읽어 보세요.
