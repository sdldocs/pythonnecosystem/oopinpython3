**객체 지향 프로그래밍(OOP, Object Oriented Programming)**은 개별 **객체(Object)**에 관련된 속성과 동작을 번들링하여 프로그램을 구성하는 방법이다. 이 튜토리얼에서는 Python에서 객체 지향 프로그래밍의 기본을 배울 것이다.

개념적으로 객체는 시스템의 구성 요소와 같습니다. 프로그램을 일종의 공장 조립 라인으로 생각해 보면, 조립 라인의 각 단계에서 시스템 구성 요소는 일부 재료를 처리하며 최종적으로 원재료를 완제품으로 변환한다.

객체에는 조립 라인의 각 단계에서 원천 또는 사전 처리된 재료와 같은 데이터와 각 조립 라인 구성요소가 수행하는 작업과 같은 동작을 포함한다.

**이 튜토리얼에서는 다음 방법에 대해 알아본다.**

- 객체를 생성하기 위한 Blueprint와 같은 **클래스** 생성
- 클래스를 사용하여 새 객체 생성
- 클래스 상속이 있는 시스템 모델링

> **Note:**  이 튜토리얼은 `Python Basics: A Practical Introduction to Python 3`[https://realpython.com/products/python-basics-book/]의 "OOP(객체 지향 프로그래밍)" 장에서 가져왔다.

이 책은 Python에 내장된 IDLE 편집기를 사용하여 Python 파일을 만들고 편집하며 Python 쉘을 사용하므로 이 튜토리얼 전체에서 가끔 IDLE에 대한 참조를 볼 수 있다. 그러나 여러분이 선택한 편집기와 개발 환경에서 예제 코드를 실행하는 데 문제는 없을 것이다.

### 목차

- [Python에서 객체 지향 프로그래밍이란?](oop.md#what-is-oop)
- [Python에서 클래스 정의](oop.md#define-class)
    - [클래스와 인스탄스](oop.md#classes-vs-instances)
    - [클래스 정의](oop.md#how-to-define-class)

- [Python에서 객체 인스탄스화](oop.md#instantiate-object)
    - [클래스와 인스탄스 속성](class-and-instance-attribute)
    - [인스탄스 메서드](instance-method)
    - [연습](check-your-understanding)

- [Python에서 다른 클래스로부터 상속](oop.md#inherit-from-other-classes)
    - [애완견 공원 예](oop.md#dog-park-example)
    - [부모 클래스와 자식 클래스](oop.md#parent-classes-vs-child-classes)
    - [부모 클래스의 기능 확장](oop.md#extend-functionality-of-parent-classes)
    - [연습](oop.md#check-your-understanding-2)

- [결론](oop.md#conclusion)

