# Object-Oriented Programming in Python3

Python의 Object-Oriented Programming Paradigm 기초에 대하여 알아 본다. 이는 많이 들었고, 다른 사람이 작성한 코드를 이해는 하지만 막상 본인이 작성하려면 잘 이행되지 않는 부분이다. 다시 한번 기초부터 알아보고 자신의 programming에 배어 있기를 바란다. 본 페이지는 David Amos가 작성한 Object-Orineted Programming in Python 3를 저자의 동의없이 개인적인 관심에서 편역한 것이다.

좋은 Python 프로그래머가 되고 싶은 사람들에게 조금이라도 도움이 되길 바란다.

created on Dec. 6, 2022
